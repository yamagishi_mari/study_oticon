const browserSync = require('browser-sync').create();
const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const plumber = require('gulp-plumber');
const pug = require('gulp-pug');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const sassGlob = require('gulp-sass-glob');
const sassVariables = require('gulp-sass-variables');
const packageImporter = require('node-sass-package-importer');
const ftp = require('vinyl-ftp');
const lcs = require('node-lcs');
const os = require('os');
const fs = require('fs');
const config = require('./gulpconfig.json');
const server = getServerInfo();
const iconfont = require('gulp-iconfont');
const consolidate = require('gulp-consolidate');

function getServerInfo() {
	try {
		return require(os.homedir() + '/gulpserver.json');
	} catch (err) {
		return false;
	}
}

gulp.task('pug', function () {
	if (config.construction == 'adaptive') {
		for (key in config.device) {
			if (config.device[key]) {
				gulp.src(['./work/pug/**/*.pug', '!./work/pug/**/_*.pug'])
					.pipe(plumber())
					.pipe(pug({
						pretty: '\t',
						locals: {
							device: key,
							construction: config.construction
						}
					}))
					.pipe(rename({
						suffix: '-' + key
					}))
					.pipe(gulp.dest('./source'))
					.pipe(browserSync.stream());
			}
		}
	}
	if (config.construction == 'responsive') {
		gulp.src(['./work/pug/**/*.pug', '!./work/pug/**/_*.pug'])
			.pipe(plumber())
			.pipe(pug({
				pretty: '\t',
				locals: {
					device: '',
					construction: config.construction
				}
			}))
			.pipe(gulp.dest('./source'))
			.pipe(browserSync.stream());
	}
});

gulp.task('sass', function () {
	if (config.construction == 'adaptive') {
		for (key in config.device) {
			if (config.device[key]) {
				gulp.src('./work/sass/**/*.scss')
					.pipe(plumber())
					.pipe(sassVariables({
						$device: key,
						$construction: config.construction
					}))
					.pipe(sassGlob())
					.pipe(sass({
						outputStyle: 'expanded',
						importer: packageImporter()
					}))
					.pipe(rename({
						suffix: '-' + key
					}))
					.pipe(gulp.dest('./common/css'))
					.pipe(browserSync.stream());
			}
		}
	}
	if (config.construction == 'responsive') {
		gulp.src('./work/sass/**/*.scss')
			.pipe(plumber())
			.pipe(sassVariables({
				$construction: config.construction
			}))
			.pipe(sassGlob())
			.pipe(sass({
				outputStyle: 'expanded',
				importer: packageImporter()
			}))
			.pipe(gulp.dest('./common/css'))
			.pipe(browserSync.stream());
	}
});

gulp.task('icon', function () {
	var fontName = 'icon';

	return gulp.src('./work/icons/*.svg')
		.pipe(iconfont({
			fontName: fontName,
			formats: ['ttf', 'eot', 'woff2', 'woff', 'svg'],
			normalize: true,
			fontHeight: 256, // 512 / 2
			descent: 36.5714285714, // 512 / 14
			appendCodepoints: true
		}))
		.on('codepoints', function (codepoints) {
			var options = {
				className: fontName,
				fontName: fontName,
				fontPath: '../fonts/',
				glyphs: codepoints
			};
			gulp.src('./work/sass/template/_icon-template.scss')
				.pipe(consolidate('lodash', options))
				.pipe(rename({
					basename: '_00_icon'
				}))
				.pipe(gulp.dest('./work/sass/element'));
			gulp.src('./work/pug/template/_icon-template.pug')
				.pipe(consolidate('lodash', options))
				.pipe(rename({
					basename: '_icon'
				}))
				.pipe(gulp.dest('./work/pug/template'));
		})
		.pipe(gulp.dest('./common/fonts'));
});

gulp.task('webserver', ['pug', 'sass'], function () {
	browserSync.init({
		notify: false,
		port: 8000,
		ui: {
			port: 8001
		},
		files: ['./common/**/*.*'],
		server: {
			baseDir: './',
			directory: true
		}
	});
	gulp.watch('./work/pug/**/*.pug', ['pug']);
	gulp.watch('./work/sass/**/*.scss', ['sass']);
	gulp.watch('./source/**/*.html').on('change', browserSync.reload);
});

gulp.task('index', function () {
	fs.readdir('./source', function (err, data) {
		if (err) throw err;
		var containerHtmlFiles = [];
		for (var i in data) {
			var regexp = new RegExp('^(?!index\.html).*\.html');
			if (data[i].match(regexp) && !data[i].match(/index\.html/g)) {
				var src = fs.readFileSync('./source/' + data[i], 'utf-8');
				var title = src.match(/<title>.*<\/title>/g);
				title = title[0].replace(/<title>/g, '').replace(/<\/title>/g, '');
				containerHtmlFiles.push({
					file: data[i],
					title: title
				});
			}
		}
		var lcsText = lcs(containerHtmlFiles[0].title, containerHtmlFiles[3].title).sequence;
		for (var i in containerHtmlFiles) {
			containerHtmlFiles[i].title = containerHtmlFiles[i].title.replace(lcsText, '');
		}
		var htmlContents = '<!DOCTYPE html><html lang=\"ja\"><head><meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"><title>' + config.company + ' ' + config.site + ' ベースコーディング</title><link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\"><style>html{font-size:.875em;}</style></head><body><div class=\"container pt-3\"><div class=\"row\"><div class=\"col-sm-12\"><h1>' + config.company + ' <small class=\"text-muted\">' + config.site + '</small></h1><p>' + config.company + ' - ' + config.site + ' - ' + config.project + 'のベースコーディングです。</p><div class=\"list-group mb-3\">\n';
		for (var i in containerHtmlFiles) {
			htmlContents += '<a href=\"' + containerHtmlFiles[i].file + '\" class=\"list-group-item list-group-item-action d-flex justify-content-between align-items-center\">' + containerHtmlFiles[i].title + ' <small class="text-monospace text-muted">' + containerHtmlFiles[i].file + '</small></a>\n';
		}
		htmlContents += '</div><dl><dt>免責事項</dt><dd>このベースコーディングは、LYZONの作業用としても使用しているため、予告なく追加・修正・削除が行われる場合があります。</dd></dl><hr><p><small>&copy; LYZON Inc.</small></p></div></div></div></body></html>';
		fs.writeFile('./source/index.html', htmlContents, function (err) {
			if (err) throw err;
		});
	});
});

gulp.task('deploy', function () {
	if (!server) {
		console.log('gulpserver.jsonが見つからないため、タスクは実行されませんでした。');
	} else {
		var conn = ftp.create({
			host: server.heteml.host,
			user: server.heteml.user,
			password: server.heteml.password
		});
		var globs = [
			'./common/**',
			'./media/**',
			'./source/**'
		];
		gulp.src(globs, {
				base: './',
				buffer: false
			})
			.pipe(conn.newerOrDifferentSize('./web/' + config.server))
			.pipe(conn.dest('./web/' + config.server));
	}
});

gulp.task('default', ['webserver']);
