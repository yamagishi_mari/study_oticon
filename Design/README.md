# TASK RUNNER

デザイナー向けコーディングサポートツール

## ABOUT

TASK RUNNERは、標準化されたテンプレートをあらかじめ用意しておくことで、初期構築にかかるコストを大幅に削減し、煩雑な作業を自動化したり、仮想サーバやファイル転送を簡単に実行するための、コーディングサポートツールです。

## REQUIREMENTS

TASK RUNNERを導入するにあたり、[制作環境構築ガイドライン](https://bitbucket.org/lyzon_co/designer/wiki/Designer%20Setup%20Guideline.md) にかかる作業を完了している必要があります。

また、サーバ情報をローカルPCに保存しておく必要があります。

`\\landisk3\disk1\LYZON業務\0_マニュアル・ノウハウ\030_デザイナー\gulpserver.json`を`C:\Users\USER_NAME`にコピーしてください。

## INSTALLATION

1. [https://bitbucket.org/lyzon_co/designer.git](https://bitbucket.org/lyzon_co/designer.git)をクローン
2. Designディレクトリ・.gitignoreを新規リポジトリにコピー
3. gulp-installer.batを実行

gulp-installer.batでは、はじめに以下の質問がされるので、それぞれ答えてください。

* クライアント名 `String`
* サイト名 `String`
* プロジェクト名 `String`
* サーバ名 `String`
* レイアウト方式 `[r]esponsive / [a]daptive`

レイアウト方式で`adaptive`を選択した場合は、デバイスごとの書き出し有無を聞かれます。

* モバイル用に書き出し `Boolean`
* タブレット用に書き出し `Boolean`
* デスクトップ用に書き出し `Boolean`

## FUNCTIONS

### Pug

PugからHTMLにコンパイルします。

* アンダースコアから始まるPugはコンパイルされません。
* インデントの既定値はタブです。
* gulpconfig.jsonのコンパイル情報に基づいて、各デバイスごとにコンパイルされます。

### Sass

SassからCSSにコンパイルします。

* ディレクトリごとにSassをインポートできます。
* 書き出しスタイルの既定値はexpandedです。
* Autoprefixerが設定に基づいて、ベンダープレフィクスを付与または削除します。
* レスポンシブ・アダプティブレイアウトの切り替えができます。
* デフォルトでフレキシブルボディ用のクラスが使用されています。

### JavaScript

CommonJS形式のJavaScriptをブラウザ参照可能なJavascriptにコンパイルします。トランスコンパイラにはWebpackを利用しています。

* jQueryはデフォルトでインストール済み。「lib/vendor.js」に出力されます。※グローバル参照も可能
* デザイン検証環境にデプロイする場合は開発用コンパイルを実行してください。（webpack-dev.bat)

### Icon

SVGからFontファイルを生成し、それに関連するPugとScssを更新します。

* 「work/icons」にsvgを1000x1000のサイズで入れてください。※サイズを統一しないと崩れが発生します。
* gulp-icon.batを叩くとFontファイルが「common/fonts」に生成され、「pug/template/_icon.pug」と「sass/element/_18_icon.scss」が更新されます。
* 上記の後にgulp-streaming.batを叩くことで、「source/x103-iconlist.html」からアイコン一覧が確認できます。
* クラス名などを変更したい場合は、「pug/template/_icon-template.pug」と「sass/template/_icon-template.scss」を編集してください。

### Webserver

仮想サーバを起動し、ファイルの変更ごとにブラウザをリロードします。

* 仮想サーバの既定値は`localhost:8000`です。
* 自機以外のデバイスから接続する場合は`192.168.31.*:8000`です。社内Ethernet/社内Wi-Fiに接続しているすべてのデバイスからアクセスできます。
* `localhost:8001`で仮想サーバの設定ができます。デバッグ設定などができます。
* PugおよびSassを監視し、これらのファイルを変更したときにコンパイル・リロードされます。
* `/common`以下のファイルが変更されたときにリロードされます。

### Index

sourceディレクトリのHTMLへのリンクをまとめた目次を生成します。

* 生成されるファイル名の既定値はindex.htmlです。

### Deploy

テスト環境にファイル転送します。

* テスト環境URLの既定値はhttp:lyzontest.heteml.jp/\*\*\*/です。
* gulpconfig.jsonのサーバ情報に基づいたディレクトリに転送します。
* 転送対象の既定値は、sourceディレクトリ・commonディレクトリ・mediaディレクトリです。
* ヘテムルの環境設定であらかじめサブドメインを割り当てておく必要があります。

## USAGE

コマンドとバッチファイルとそのタスクは以下の通りです。

|        コマンド       |                                                 タスク                                                  |   バッチファイル   |
| --------------------- | ------------------------------------------------------------------------------------------------------- | ------------------ |
| `$ npm run default`   | 仮想サーバ起動・ブラウザ自動更新・Pug自動コンパイル・Sass自動コンパイル                                 |                    |
| `$ npm run pug`       | Pugコンパイル                                                                                           |                    |
| `$ npm run sass`      | Sassコンパイル                                                                                          |                    |
| `$ npm run icon`      | IconFont生成                                                                                          |                    |
| `$ npm run wp-dev`    | JS自動コンパイル（開発環境用）                                                                          | webpack-dev.bat    |
| `$ npm run wp-dev-w`  | JSコンパイル（開発環境用）                                                                              |                    |
| `$ npm run wp-prod-w` | JSコンパイル（本番環境用）                                                                              | webpack-prod.bat   |
| `$ npm run index`     | 目次（index.html）作成                                                                                  |                    |
| `$ npm run test`      | Pugコンパイル・Sassコンパイル・目次（index.html）作成                                                   | gulp-test.bat      |
| `$ npm run deploy`    | ファイル転送                                                                                            |                    |
| `$ npm run webserver` | 仮想サーバ起動・ブラウザ自動更新・Pug自動コンパイル・Sass自動コンパイル                                 |                    |
| `$ npm run streaming` | 仮想サーバ起動・ブラウザ自動更新・Pug自動コンパイル・Sass自動コンパイル・JS自動コンパイル（開発環境用） | gulp-streaming.bat |
| `$ npm run release`   | 目次（index.html）作成・ファイル転送                                                                    | gulp-release.bat   |

**注意**

* gulp-watch.batまたはgulp-streaming.batの各バッチファイルを終了する際は、先に`Ctrl + C`で処理を停止し、その後ウィンドウの閉じてください。タスク実行中のままウィンドウの閉じると強制終了扱いとなり、エラーが発生する場合があります。
* Gitを扱う際は、必ずすべてのコマンドまたはバッチファイルを終了してください。

## ISSUE

TASK RUNNERに関する課題は[https://bitbucket.org/lyzon_co/designer/issues/](https://bitbucket.org/lyzon_co/designer/issues/)で受け付けています。

バグ修正やテンプレート追加などは、各自の判断で自由にコミットできます。フォークやプルリクエストなどのルールはありません。
