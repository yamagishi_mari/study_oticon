/**
 * developモードのみ develop.jsを取り込む
 */
!PRODUCTION ? require('develop') : null;

import 'slick-carousel/slick/slick.min.js';

$(function() {
    $('.autoplay').slick({
        autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '<div class="slick_prev"></button>',
        nextArrow: '<div class="slick_next"></button>',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });
});

/**
 * ドロップダウンメニュー
 */
$(function(){
    $('.dropdwn_list').on('click',function(){
        $('.dropdwn_menu').toggleClass('is-active');
    })
});

/**
 * メニュー全体を表示非表示切り替え
 */
$(function(){
    $('.menuBtn').on('click', function(){
        $(this).addClass('is-active');
        $('.drawerMenu').addClass('is-active');
        $('body').addClass('is-active');
/**
 * 閉じるボタンを非表示から表示に変える
 */
        $('.closeMenu_btn').addClass('is-active');
    });

    $('.closeMenu_btn').on('click', function(){
        $('.drawerMenu').scrollTop(0);
        $(this).removeClass('is-active');
        $('.drawerMenu').removeClass('is-active');
        $('body').removeClass('is-active');
    });

    $('.closeMenu2').on('click', function(){
        $('.drawerMenu').scrollTop(0);
        $('.closeMenu_btn').removeClass('is-active');
        $('.drawerMenu').removeClass('is-active');
        $('body').removeClass('is-active');
    });

});
/**
 * ページの指定された位置に移動する
 */
$(function(){
    $('.pageTopBtn').click(function(){
        $('html').animate({scrollTop: 0});
    });
});
