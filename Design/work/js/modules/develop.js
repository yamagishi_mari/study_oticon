var hljs = require('highlightjs');
var Clipboard = require('clipboard');

/**
 * 開発環境確認用 JS
 */
module.exports = (function() {

	$(function() {

		if ($('.dev-sectionView')[0]) {
			$('.dev-sectionView').each(function(index, el) {
				var e = $(this).html();
				var f = $('<div>').text(e).html().replace(/\n\t\t\t\t\t/g, '\n').replace(/\t/g, '  ').trim();
				// var g = $(this).find('[class*="col-"]').attr('class').replace(/[0-9]{2}/g, '12');
				$(this).after('<div class="dev-sectionCode"><div class="fxb-section"><div class="fxb-container"><div class="fxb-row"><div class="fxb-col-12"><pre><code class="html" id="code_' + index + '"></code><button class="dev-clipboard" data-clipboard-target="#code_' + index + '">COPY</button></pre></div></div></div></div></div>');
				$(this).next('.dev-sectionCode').find('code').html(f);
			});
		}
		if ($('.dev-elementView')[0]) {
			$('.dev-elementView').each(function(index, el) {
				var e = $(this).html();
				var f = $('<div>').text(e).html().replace(/\n\t\t\t\t\t\t\t\t/g, '\n').replace(/\t/g, '  ').trim();
				var g = $(this).attr('class').replace(/View/g, 'Code');
				$(this).after('<div class="' + g + '"><pre><code class="html" id="code_' + index + '"></code><button class="dev-clipboard" data-clipboard-target="#code_' + index + '">COPY</button></pre>');
				$(this).next('.dev-elementCode').find('code').html(f);
			});
		}
		if ($('.dev-hashList')[0]) {
			$('.dev-h2').each(function(index, el) {
				$(this).attr('id', 'element_' + index);
				$('.dev-hashList').append('<a href="#element_' + index + '">' + $(this).text() + '</a>');
			});
		}
		if ($('.dev-clipboard')[0]) {
			var clipboard = new Clipboard('.dev-clipboard');
			clipboard.on('success', function(event) {
				event.clearSelection();
			});
		}
		if ($('.dev-developerMode')[0]) {
			var c = 0;
			$('.dev-developerMode').on('click', function(event) {
				event.preventDefault();
				c++;
				if (c == 7) {
					var d = confirm('本当に開発者モードにしますか？');
					if (d) {
						$('.dev-secret').show();
					} else {
						c = 0;
					}
				}
			});
		}

		hljs.initHighlightingOnLoad();

	});

})();