@echo off

cd /d %~dp0

title Task Runner Setup Manager

if not exist gulpconfig.json (
	echo # Please write project information ...
	set /P CLIENT_NAME="Client Name (String):"
	set /P SITE_NAME="Site Name (String):"
	set /P PROJECT_NAME="Project Name (String):"
	set /P SERVER_NAME="Server Name (String):"
	:CONSTRUCTION_CHECK
	set /P CONSTRUCTION_VAR="Construction ([r]esponsive / [a]daptive):"
	if "%CONSTRUCTION_VAR%"=="r" (
		set CONSTRUCTION=responsive
		set DEVICE_DESKTOP=false
		set DEVICE_TABLET=false
		set DEVICE_MOBILE=false
	) else if "%CONSTRUCTION_VAR%"=="a" (
		set CONSTRUCTION=adaptive
		set /P DEVICE_DESKTOP="Compile Desktop? (Boolean):"
		set /P DEVICE_TABLET="Compile Tablet? (Boolean):"
		set /P DEVICE_MOBILE="Compile Mobile? (Boolean):"
	) else (
		goto CONSTRUCTION_CHECK
	)
)

chcp 65001

if not exist gulpconfig.json (
	echo # Create gulpconfig.json ...
	echo {>> gulpconfig.json
	echo 	^"company^": ^"%CLIENT_NAME%^",>> gulpconfig.json
	echo 	^"site^": ^"%SITE_NAME%^",>> gulpconfig.json
	echo 	^"project^": ^"%PROJECT_NAME%^",>> gulpconfig.json
	echo 	^"server^": ^"%SERVER_NAME%^",>> gulpconfig.json
	echo 	^"construction^": ^"%CONSTRUCTION%^",>> gulpconfig.json
	echo 	^"device^": {>> gulpconfig.json
	echo 		^"desktop^": %DEVICE_DESKTOP%,>> gulpconfig.json
	echo 		^"tablet^": %DEVICE_TABLET%,>> gulpconfig.json
	echo 		^"mobile^": %DEVICE_MOBILE%>> gulpconfig.json
	echo 	}>> gulpconfig.json
	echo }>> gulpconfig.json
)

findstr "devDependencies dependencies" package.json
if "%ERRORLEVEL%"=="0" (
	echo # Install packages from package.json ...
	call npm i
) else if "%ERRORLEVEL%"=="1" (
	echo # Create package.json and Install packages ...
	call npm i -D npm-run-all browser-sync gulp@3.9.1 gulp-autoprefixer gulp-plumber gulp-pug gulp-rename gulp-sass gulp-sass-glob gulp-sass-variables vinyl-ftp cross-env webpack@3.12.0 expose-loader clipboard highlightjs gulp-consolidate@0.1.2 gulp-iconfont@1.0.0 lodash@2.4.1 node-lcs
	call npm i jquery --save
)

echo # Create directory ...
md common\css
md common\images
md common\fonts
md media
md document
md work\pug\base
md work\pug\layout
md work\pug\module
md work\pug\component
md work\sass\base
md work\sass\layout
md work\sass\module
md work\sass\component
md work\icons

echo Setup was finished successfully.

pause
