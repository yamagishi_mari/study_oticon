const webpack = require("webpack");
const isProduction = process.env.NODE_ENV === 'production';

/**
 * webpack@3.x系 config
 */
module.exports = {

    // entryとなるjsのパス、出力するパスを記述する
    entry: {
        // 共通ライブラリ
        'vendor': ['jquery'],
        // 全体で読み込むエントリー
        'common/common' : './work/js/entry/common/common.js',
    },

    // 出力先を指定する
    output: {
        path: __dirname + '/common/js/', // root
        filename: '[name].js' // entry keyがnameに入る
    },

    module: {
        // loaderを登録する
        rules: [
            // jQueryをグローバルに登録する
            {
                test: require.resolve('jquery'),
                loader: 'expose-loader?jQuery!expose-loader?$'
            }
        ]
    },

    resolve: {
        // エイリアスを登録する
        alias: {
            // デザイン開発環境用
            'develop' : __dirname + '/work/js/modules/develop.js',
        }
    },

    plugins: [
        // vendorを外部ファイルにする
        new webpack.optimize.CommonsChunkPlugin({
            name: "vendor",
            filename: "lib/vendor.js",
            minChunks: Infinity
        }),
        // jqueryを全てのエントリーポイントで読みように設定
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            IScroll: 'iscroll',
        }),
        // ビルド時に使用する変数を定義
        new webpack.DefinePlugin({
            PRODUCTION : isProduction ? true : false
        })
    ]

};

// 開発用
if (!isProduction) {
    // ソースマップの指定
    module.exports.devtool = 'eval-source-map';
}

// 本番用
if (isProduction) {
    module.exports.plugins.push(
      // minify
      new webpack.optimize.UglifyJsPlugin({
          compress: {
              // warnings を消す
              warnings: false,
              // console.log を消す
              drop_console: true
          }
      })
   );
}
