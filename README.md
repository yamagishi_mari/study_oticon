# プロジェクト仕様書

## 環境

|       Name       |                                    URL                                     |           Branch           |             Delpoy            |
| ---------------- | -------------------------------------------------------------------------- | -------------------------- | ----------------------------- |
| デザイン確認環境 | [http://lyzoncorporate.lyzontest.com](http://lyzoncorporate.lyzontest.com) | develop / staging / master | gulp command / git pull / FTP |
| 開発環境         | [http://lyzoncorp.lyzonserver01/](http://lyzoncorp.lyzonserver01/)         | develop / staging / master | gulp command / git pull / FTP |
| 検証環境         | [https://lyzoncorp-cms.aws/](https://lyzoncorp-cms.aws/)                   | develop / staging / master | gulp command / git pull / FTP |
| 本番環境         | [https://lyzon.co.jp/](https://lyzon.co.jp/)                               | develop / staging / master | gulp command / git pull / FTP |
| その他           | -                                                                          | -                          | -                             |

## 技術仕様

|      Name     |               Description                |
| ------------- | ---------------------------------------- |
| CMS           | Sitecore / WordPress / MobaleType        |
| HTML          | HTML 5                                   |
| CSS           | CSS3                                     |
| CSS Framework | Bootstrap / Foundation / 960 Grid System |
| Multi Device  | レスポンシブ / アダプティブ              |
| その他        | -                                        |

### 対応OSおよびブラウザ

|                      | Windows 7 SP1 | Windows 8.1 Update | Windows 10 | OS X | iOS | Android |
| -------------------- | ------------- | ------------------ | ---------- | ---- | --- | ------- |
| Internet Explorer 11 | *             | *                  | O          | -    | -   | -       |
| Edge                 | -             | -                  | O          | -    | -   | -       |
| Chrome               | *             | *                  | O          | *    | *   | O       |
| Safari               | -             | -                  | -          | O    | O   | -       |
| Firefox              | *             | *                  | O          | *    | *   | *       |

* `O` : 対応する
* `*` : 対応するが原則として実機確認は行わない
* `-` : 対応しない または対応する条件が存在しない

**この表について** : 記載されている各OS/ブラウザのバージョンの基準は、コーディングガイドラインを参照してください。

### 対応デバイスおよび画面解像度

|   デバイス名   | 画面解像度  | ブレイクポイント(レスポンシブ) | デバイス判定(アダプティブ) |
| -------------- | ----------- | ------------------------------ | -------------------------- |
| デスクトップ   | 1920x1080px | xl                             | desktop                    |
| ノートパソコン | 1366x768px  | lg                             | laptop                     |
| タブレット     | 768x1024px  | md                             | tablet                     |
| スマートフォン | 375x667     | xs(portrait) / sm(landscape)   | mobile                     |

## 各種資料

|       Name       |                                              Description                                              |
| ---------------- | ----------------------------------------------------------------------------------------------------- |
| WBS              | [https://jp.smartsheet.com/](https://jp.smartsheet.com/)                                              |
| ワイヤーフレーム | [https://lyzon.hotgloo.com/dashboard/](https://lyzon.hotgloo.com/dashboard/)                          |
| デザイン         | `\\landisk3\disk1\企業向けWEB対策ビジネス\20\_顧客データ（契約後）\b00000\_プロジェクト\60_デザイン\` |
| システム全体像   |                                                                                                       |
| インフラ構成     |                                                                                                       |
| その他           | -                                                                                                     |

### 対応項目

|  対応名  | 対応 | 備考 |
| -------- | ---- | ---- |
| GDPR対応 |      |      |

* `O` : 対応する
* `*` : 対応するが原則として実機確認は行わない
* `-` : 対応しない または対応する条件が存在しない

# エンジニア共有用項目

## 環境構築手順

- Librariesに含めるべきdll等を適宜記載

## VisualStudioソリューション構成等（別IDEの場合書き換え）

|          Name          | Description | other |
| ---------------------- | ----------- | ----- |
| Lyzon.CommonWebControl |             |       |
| Lyzon.SitecoreControl  |             |       |

## CMS使用フレームワークやモジュール等

| Name | Version | other |
| ---- | ------- | ----- |
| WFFM |         |       |

# デザイナー共有用項目

# ディレクター共有用項目

# その他共有項目
